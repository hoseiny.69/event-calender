# Laravel (lumen) based calender API

This project has been built by lumen version 5.6 and has some configurations which I reperesent 
here for deploying project on any kind of server

## Server Deploy Documentation
Whenever you want to deploy project on your server you need to know : 

1-you should config this files for your own server : 
    
    calender/.env, calender/config/conf.php

Since this project is developed using lumen you should NOT run below commands: 
    
    php artisan config:clear
    php artisan config:cache

2-To make project able to run you need to run this command to give proper permission to storage directory :
    
    sudo chmod 777 storage/ -R
    
3-If project dosen't have based64 generate key you need to generate new one :

    php artisan key:generate
       
4-You need to make sure you have Client Access Token in your project and database if you don't Create New one : 
    
    php artisan passport:client --password
    
For Apache webServers we need this configuration :

    nano .htaccess  
   ------------------------------------
    <IfModule mod_rewrite.c>
        <IfModule mod_negotiation.c>
            Options -MultiViews
        </IfModule>
    
        RewriteEngine On
    
        # Redirect Trailing Slashes If Not A Folder...
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteRule ^(.*)/$ /$1 [L,R=301]
    
        # Handle Front Controller...
        RewriteCond %{REQUEST_FILENAME} !-d
        RewriteCond %{REQUEST_FILENAME} !-f
        RewriteRule ^ index.php [L]
    
        # Handle Authorization Header
        RewriteCond %{HTTP:Authorization} .
        RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
    </IfModule>

If composer faild, you need to install composer, don't forget that using :
 
    composer install
   
   
   ## Domain
Don't Forget to define domain for the api like: 

    127.0.0.1 api.calender.local
   
      
## WIKI
   Please Checkout wiki section for postman collection + environment for testing the project