<?php

namespace Tests\Unit\User;

use App\Models\User;
use Hash;
use Tests\TestCase;

class UserTest extends TestCase
{
    
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testUserCreated()
    {
        $user = factory(User::class)->create();
        $this->assertDatabaseHas('users', ['id' => $user->id]);
    }

    public function testUSerUpdatedSuccessfully(Faker\Generator $faker)
    {
        $user = factory(User::class)->create();
        $new_name = $faker->name;

        $user->name = $new_name;
        $user->save();

        $this->assertDatabaseHas('users', ['id' => $user->id, 'name' => $user->name]);
    }

    public function testUserMassInsert(Faker\Generator $faker)
    {
        $count = $faker->numberBetween(2, 20);
        $users = factory(User::class, $count)->create();

        $this->assertEquals($count,$users->count());
        $this->assertTrue(User::count() >= $count);
    }
    
    public function test_password_is_being_hashed(Faker\Generator $faker)
    {
        $user = factory(User::class)->make();
        $user->password = $password = $faker->password;
        $this->assertNotEquals($password, $user->password);
        $this->assertTrue(Hash::check($password, $user->password));
    }

    public function test_mass_assignment_values(Faker\Generator $faker)
    {
        $user = factory(User::class)->make();
        $data = [
            'name'        => $faker->name,
            'mobile'      => $faker->mobile,
            'password'    => $faker->password,
            'ip_address' => $faker->ip,
        ];

        $user->fill($data);
        $this->assertArrayNotHasKey('extra_field', $user->toArray());
    }

    public function test_user_birthday_is_null()
    {
        $user = factory(User::class)->create();
        $this->assertNotNull($user->birthday);
        $user->birthday = null;
        $this->assertNull($user->birthday);
        $user->save();
        $user->refresh();
        $this->assertNull($user->day);
        $this->assertNull($user->month);
        $this->assertNull($user->year);
    }

    public function test_user_gender_accessor_and_mutator()
    {
        $user= factory(User::class)->create();
        $user->gender= USER_GENDER_FEMALE;
        $this->assertEquals(USER_GENDER_FEMALE ,$user->gender);
        $user->gender = USER_GENDER_MALE;
        $user->save();
        $this->assertEquals(USER_GENDER_MALE,$user->gender);
    }
}
