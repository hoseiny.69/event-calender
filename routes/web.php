<?php
    
    /*
    |--------------------------------------------------------------------------
    | Application Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register all of the routes for an application.
    | It is a breeze. Simply tell Lumen the URIs it should respond to
    | and give it the Closure to call when that URI is requested.
    |
    */
    
    /*
    |--------------------------------------------------------------------------
    | Api Routes and Endpoints
    |--------------------------------------------------------------------------
    * Api Routes
    */
    $router->group(['prefix' => 'api', 'middleware' => ['cors']], function () use ($router) {
        
        /*
        |--------------------------------------------------------------------------
        | Register and Login Routes and Endpoints
        |--------------------------------------------------------------------------
         * Authentications Routes
         */
        $router->post('/login', 'Auth\AuthController@login');
        
        //User Logout
        $router->post('/logout', 'Auth\AuthController@logout');
        
        /*
        |--------------------------------------------------------------------------
        | Users Routes and Endpoints
        |--------------------------------------------------------------------------
        * Routes for resource Users
        */
        $router->group(['prefix' => 'user', 'middleware' => ['role:' . NORMAL_USER_ROLE, 'auth:api']], function () use ($router) {
            
            $router->get('/', ['uses' => 'Auth\AuthController@get_user']);
        });
        
        /*
        |--------------------------------------------------------------------------
        | Events Routes and Endpoints
        |--------------------------------------------------------------------------
        * Routes for resource Events
        */
        $router->group(['prefix' => 'user/events', 'middleware' => ['role:' . NORMAL_USER_ROLE, 'auth:api']], function () use ($router) {
            
            $router->get('/', ['uses' => 'User\EventsController@index']);
            $router->get('/{id}', ['uses' => 'User\EventsController@show']);
            $router->post('/', ['uses' => 'User\EventsController@store']);
            $router->post('/{id}', ['uses' => 'User\EventsController@update']);
            $router->delete('/{id}', ['uses' => 'User\EventsController@destroy']);
        });
        
    });