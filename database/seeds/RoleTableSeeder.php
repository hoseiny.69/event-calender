<?php

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions
        Permission::create(['name' => CAN_READ]);
        Permission::create(['name' => CAN_EDIT]);
        Permission::create(['name' => CAN_DELETE]);
        Permission::create(['name' => ALL_PRIVILAGES]);

        // create roles and assign created permissions
        $AdminRole = Role::create(['name' => ADMIN_ROLE]);
        $AdminRole->givePermissionTo(ALL_PRIVILAGES);

        $NormalRole = Role::create(['name' => NORMAL_USER_ROLE]);
        $NormalRole->givePermissionTo([CAN_READ, CAN_EDIT, CAN_DELETE]);

    }
}
