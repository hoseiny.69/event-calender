<?php

use App\Models\OAuthClient;
use Illuminate\Database\Seeder;

class OAuthClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $ouathClient = new OauthClient();
        $ouathClient->user_id = '1';
        $ouathClient->name = 'Password Grant Client';
        $ouathClient->secret = '5X6cwIm4Wpjf3HAfIQQNaGF3unY1ANerEwJTxUBV';
        $ouathClient->redirect = 'http://localhost'; //Getting app url path from conf file;
        $ouathClient->personal_access_client = '1';
        $ouathClient->password_client = '1';
        $ouathClient->revoked = '0';
        $ouathClient->save();
    }
}
