<?php
    
    use App\Models\User;
    use Illuminate\Database\Seeder;
    
    class UserTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run(Faker\Generator $faker)
        {
            $hasher = app()->make('hash');
            
            $admin = new User();
            $admin->name = $faker->name;
            $admin->birth_date = $faker->dateTime;
            $admin->mobile = str_pad(mt_rand(9, 99999), 10, '0', STR_PAD_LEFT);
            $admin->mobile_confirmed = true;
            $admin->password = $hasher->make("1234567890");
            $admin->ip_address = $faker->ipv4;
            $admin->is_activated = true;
            $admin->save();
            $admin->assignRole(ADMIN_ROLE);
            
            $normal_user = new User();
            $normal_user->name = $faker->name;
            $normal_user->birth_date = $faker->dateTime;
            $normal_user->mobile = str_pad(mt_rand(9, 99999), 10, '0', STR_PAD_LEFT);;
            $normal_user->mobile_confirmed = true;
            $normal_user->password = $hasher->make("1234567890");
            $normal_user->ip_address = $faker->ipv4;
            $normal_user->is_activated = true;
            $normal_user->save();
            $normal_user->assignRole(NORMAL_USER_ROLE);
            
            $promotion_user = new User();
            $promotion_user->name = $faker->name;
            $promotion_user->birth_date = $faker->dateTime;
            $promotion_user->mobile = str_pad(mt_rand(9, 99999), 10, '0', STR_PAD_LEFT);;
            $promotion_user->mobile_confirmed = true;
            $promotion_user->password = $hasher->make("1234567890");
            $promotion_user->ip_address = $faker->ipv4;
            $promotion_user->is_activated = true;
            $promotion_user->save();
            $promotion_user->assignRole(NORMAL_USER_ROLE);
            
        }
    }
