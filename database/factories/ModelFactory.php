<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Posts::class, function (Faker\Generator $faker) {
    return [
        'peoples' => mt_rand(1, 10),
        'location' => $faker->paragraph(4),
        'contents' => $faker->paragraph(4),
        'tags' => mt_rand(1, 10),
        'likes' => mt_rand(1, 10),
        'caption' => $faker->paragraph(4),
        'is_comment_enable' => false,
        'user_id' => mt_rand(1, 10)
    ];
});
$factory->define(App\Models\Comments::class, function (Faker\Generator $faker) {
    return [
        'comment' => $faker->paragraph(1),
        'likes' => mt_rand(1, 50),
        'user_id' => mt_rand(1, 10),
        'reply_to' => mt_rand(1, 10)
    ];
});
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $hasher = app()->make('hash');
    return [
        'group_id' => mt_rand(1, 10),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'birth_date' => $faker->dateTime,
        'mobile' => mt_rand(1, 50),
        'mobile_confirmed' => true,
        'tel' => mt_rand(1, 50),
        'city_id' => mt_rand(1, 10),
        'bio' => $faker->paragraph(1),
        'email' => $faker->email,
        'email_confirmed' => true,
        'password' => $hasher->make("secret"),
        'ip_address' => $faker->ipv4,
        'activation_code' => mt_rand(1, 10),
        'is_superuser' => false,
        'lat' => mt_rand(1, 50),
        'long' => mt_rand(1, 50),
        'is_activated' => true,
        'detail_id' => mt_rand(1, 10),
        'saved_post' => mt_rand(1, 10),
        'searches' => mt_rand(1, 10),
        'reagent_code' => mt_rand(1, 10),
        'reagent_id' => mt_rand(1, 10),
        'discount' => mt_rand(1, 10),
        'blocked_users' => mt_rand(1, 10),
        'is_pet_owner' => false,
    ];
});
