<?php
    
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;
    
    class CreateUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->charset = 'utf8';
                
                $table->increments('id');
                $table->string('name', 255)->nullable();
                $table->enum('gender', [USER_GENDER_MALE, USER_GENDER_FEMALE])->default(USER_GENDER_MALE);
                $table->date('birth_date')->nullable();
                $table->string('mobile', 50)->unique()->nullable();
                $table->text('password')->nullable();
                $table->string('ip_address', 255)->nullable();
                $table->tinyInteger('mobile_confirmed', false, true)->default(0);
                $table->tinyInteger('is_activated', false, true)->default(0);
                $table->tinyInteger('is_deactive')->default(0);
                $table->timestamp('expiration_at')->nullable();
                $table->timestamp('deleted_at')->nullable();
                $table->timestamps();
            });
        }
        
        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('users');
        }
    }
