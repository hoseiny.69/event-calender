<?php

namespace App\Providers;

use Carbon\Carbon;
use Dusterio\LumenPassport\LumenPassport;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {
        LumenPassport::routes($this->app);

        LumenPassport::allowMultipleTokens();

        // Second parameter is the client Id
        LumenPassport::tokensExpireIn(Carbon::now()->addYears(50), 2);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'production') {
            $this->app->singleton(‘mailer’, function ($app) {
                $app->configure(‘services’);
                return $app->loadComponent(‘mail’, ‘Illuminate\Mail\MailServiceProvider’, ‘mailer’);
            });
        }
    }


}
