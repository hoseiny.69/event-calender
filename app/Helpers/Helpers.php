<?php
    
    use App\Mail\TokenConfirmation;
    use App\Models\OauthAccessToken;
    use App\Models\User;
    use Illuminate\Http\Request;
    
    /*
    |--------------------------------------------------------------------------
    | Check if public_path is not defines then define it in here Create Unique Id based on microtime Function
    |--------------------------------------------------------------------------
    */
    if (!function_exists('public_path')) {
        /**
         * Get the configuration path.
         *
         * @param  string $path
         *
         * @return string
         */
        function public_path($path = null)
        {
            return rtrim(app()->basePath('public/' . $path), '/');
        }
    }
    
    /**
     * @param $user_id
     *
     * @return mixed
     */
    function Remove_Access_Token($user_id)
    {
        $res = OauthAccessToken::whereUserId($user_id)->delete();
        return $res;
    }
    
    //issue user access token
    /**
     * @param $user
     *
     * @return mixed
     */
    function GetToken($user_object)
    {
        if ($user_object) {
            $token = $user_object->createToken('Laravel Password Grant Client')->accessToken;
            
            //creating a collection for response
            $result_collection = collect();
            $result_collection->offsetSet('token_type', "Bearer"); // similar to add function but with index
            $result_collection->offsetSet('access_token', $token); // similar to add function but with index
            $result_collection->offsetSet('user', $user_object); // similar to add function but with index
            return $result_collection;
        } else {
            abort(404, 'User does not exist');
        }
    }
    
    function RemoveZeroFromMobile(Request $request)
    {
        if ($request->has('mobile')) {
            //Get all request data
            $requestData = $request->all();
            // remove begining zeros from mobile number
            $requestData['mobile'] = ltrim($requestData['mobile'], '\r\t\n\0');
            //replace request data with new one
            $request->replace($requestData);
        }
    }
    
    function AddOldUsername(Request $request, $id)
    {
        //Get all request data
        $requestData = $request->all();
        // remove begining zeros from mobile number
        $requestData['old_username'] = User::whereId($id)->value('username');
        //replace request data with new one
        $request->replace($requestData);
    }