<?php
    
    
    /*
    |--------------------------------------------------------------------------
    | Default System Password
    |--------------------------------------------------------------------------
    */
    define('NULL_VALUE', 'null');//NULL_VALUE
    define('REMOVE', 'REMOVE');//REMOVE
    define('EMPTY_STRING', '');//EMPTY_STRING
    
    /*
    |--------------------------------------------------------------------------
    | System Messages
    |--------------------------------------------------------------------------
    */
    define('CODE_401', 'Unauthorized');//Unauthorized
    define('CODE_405_MethodNotAllowed', 'Method Not Allowed.');//MethodNotAllowedHttpException
    define('CODE_404_NotFound', 'Route Not Found.');//NotFoundHttpException
    
    /*
    |--------------------------------------------------------------------------
    | Rules params
    |--------------------------------------------------------------------------
    */
    define('RULES_404', 'No Rules have found.');
    define('RULES_400', 'Something went wrong while storing rules.');
    define('RULES_STORE_200', 'Rules stored successfully.');
    define('RULES_UPDATE_200', 'Rules updated successfully.');
    
    
    /*
    |--------------------------------------------------------------------------
    | User Model Messages
    |--------------------------------------------------------------------------
    */
    define('USER_AUTH_401', 'Your credentials are invalid!');//Your email or password incorrect!
    define('USER_ID_404', 'No user info found with the given id');//No user info found with the given id
    define('USER_404', 'No user info found.');//No user info found.
    define('USER_AUTH_404', 'No activation code found');//No activation code found
    define('USER_REGISTER_200', 'You registered successfully.');//You registered successfully
    define('USER_RESET_PASS_GET_TOKEN_200', 'User reset password token has been sent.');//User reset password token has been sent.
    define('USER_RESET_PASS_EXPIRED_406', 'User reset password token has been expired.');//User reset password token has been expired.
    define('USER_RESET_PASS_SUCCES_200', 'User password reset successfully.');//User password reset successfully.
    define('USER_RESET_INVALID_INPUT_406', 'User input are incorrect.');//User input are incorrect..
    define('USER_ACTIVE_200', 'Your account is activated.');//activating user account successfully.
    define('USER_ACTIVE_406', 'Your account is not activated.');//user account is not activated.
    define('USER_ACTIVE_401', 'Activation code is expired.');//Activation code is expired.
    define('USER_ACTIVE_400', 'Activation code is not expired.');//Activation code is not expired.
    define('USER_ACTIVECODE_406', 'Activation code is invalid.');//Activation code is invalid
    define('USER_ACTIVECODE_200', 'Resend Activation code successfully.');//Resend Activation code successfully
    define('USER_UPDATE_MOBILE_406', 'Can not change confirmed mobile parameter.');//Can not change confirmed mobile parameter.
    define('USER_UPDATE_EMAIL_406', 'Can not change confirmed email parameter.');//Can not change confirmed email parameter.
    define('USER_ACTIVECODE_MULTIPLE_406', 'Access_Token is already issued and responded, user account is activated.');//Access_Token is already issued and responded, user account is activated
    define('USER_IS_ACTIVE_400', 'User account is already activated.');//User account is already activated.
    define('USER_LOGOUT_200', 'Logged out successfully.');//User account is log out.
    define('USER_DEL_200', 'The user has been deleted successfully.');//The user has been deleted.
    define('USER_UPDATE_200', 'The user has been updated successfully.');//The user has been updated.
    define('USER_UPDATE_PASS_200', 'The user password has been updated successfully.');//The user password has been updated.
    define('USER_PASS_UPDATE_406', 'The old password entry is incorrect.');//The old password entry is incorrect.
    define('USER_UNBLOCKED_200', 'The target user has been unblocked.');//The target user has been unblocked.
    define('USER_DEACTIVE_200', 'The user is deactivated.');//The user is deactivate.
    
    /*
    |--------------------------------------------------------------------------
    | User role values
    |--------------------------------------------------------------------------
    */
    define('ADMIN_ROLE', 'ADMIN_USER');//Admin user role
    define('NORMAL_USER_ROLE', 'NORMAL_USER');//Normal user role
    
    
    /*
    |--------------------------------------------------------------------------
    | Event Calender values
    |--------------------------------------------------------------------------
    */
    define('EVENT_STORE_200', 'Event created successfully.');
    define('EVENT_UPDATE_200', 'Event updated successfully.');
    define('EVENT_ID_404', 'No event has found with the given id.');
    define('NO_EVENT_404', 'You have no event.');
    define('EVENT_DEL_200', 'Event deleted successfully.');
    
    
    /*
    |--------------------------------------------------------------------------
    | User Permissions
    |--------------------------------------------------------------------------
    */
    define('CAN_READ', 'can read');//user can read
    define('CAN_EDIT', 'can edit');//user can edit
    define('CAN_DELETE', 'can delete');//user can delete
    define('ALL_PRIVILAGES', 'all privilages');//user have access to all permissions
    
    
    /*
    |--------------------------------------------------------------------------
    | User Const Params
    |--------------------------------------------------------------------------
    */
    define('USER_GENDER_MALE', 'MALE');//User Male Gender
    define('USER_GENDER_FEMALE', 'FEMALE');//User FeMale Gender

