<?php
    
    namespace App\Http\Controllers\User;
    
    use App\Models\AnimalBreeds;
    use App\Models\BlockedUsers;
    use App\Models\Likes;
    use App\Models\Posts;
    use App\Models\Tags;
    use App\Models\User;
    use App\Http\Controllers\Controller;
    use App\Models\UserRole;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Validation\Rule;
    use Validator;
    use DB;
    
    class UserController extends Controller
    {
        protected static $StatusCode = 0;
        protected static $Error;
        protected static $Msg = null;
        protected static $Data = null;
        
        
        public function index()
        {
            try {
                $user_id = Auth::user()->id;
                $user = User::find($user_id);
                
                $user->posts_count = $user
                    ->posts()
                    ->count();
                
                $user->followers_count = $user
                    ->followers()
                    ->where("accepted", 1)
                    ->count();
                
                $user->followings_count = $user
                    ->followings()
                    ->where("accepted", 1)
                    ->count();
                
                $user->can_edit_profile = ($user->id == $user_id) ? 1 : 0;
                $user->can_see_private_info = 0;
                if (
                    $user->is_private == 0 ||
                    $user->follow_status == "follow" ||
                    $user->id == $user_id
                )
                    $user->can_see_private_info = 1;
                
                if ($user->can_see_private_info == 1)
                    $user->load([
                        "posts" => function ($query) {
                            $query->with([
                                'user' => function ($query) {
                                    $query->select(
                                        "id",
                                        "username",
                                        "name",
                                        "profile_photo"
                                    )
                                        ->withCount('posts')
                                        ->withCount('followers')
                                        ->withCount('followings');
                                }])
                                ->with(["tags", "peoples"])
                                ->withCount('likes')
                                ->withCount('comments')
                                ->take(30)
                                ->orderBy('created_at', 'desc');
                        }
                    ]);
                else
                    $user->posts = false;
                
                $user->pet_types = $user->PetTypes();
                $user->account_types = [PROMOTION_USER_PET_SHOP, PROMOTION_USER_PET_CLINIC];
                
                //prepare response
                self::$StatusCode = 200;
                self::$Error = false;
                self::$Msg = null;
                self::$Data = $user;
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function update(Request $request)
        {
            try {
                $user_id = Auth::user()->id;
                RemoveZeroFromMobile($request);
                //Get old username if it's not null
                AddOldUsername($request, $user_id);
                
                $validator = Validator::make($request->all(), [
                    'username' => [
                        'required_without:old_username',//check for old username if it's null then return error which it's require
                        Rule::unique('users')->ignore($user_id)
                    ],
                    'account_type' => Rule::in([
                        PROMOTION_USER_PET_SHOP,
                        PROMOTION_USER_PET_CLINIC
                    ]),
//                    'profile_photo' => 'mimes:jpg,jpeg,png,JPG,JPEG,PNG',
                    'birth_date' => 'date',
                    'mobile' => [
                        Rule::unique('users')->ignore($user_id)
                    ],
                    'email' => [
                        'email',
                        Rule::unique('users')->ignore($user_id)
                    ],
                    'website' => 'regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?.*/',
                    'gender' => Rule::in([
                        USER_GENDER_MALE,
                        USER_GENDER_FEMALE
                    ]),
                    'password' => 'min:6|confirmed',
                    'county_id' => 'exists:counties,id',
                    'pet_type' => 'required_with:breeds|exists:animal_categories,id',
                    'receive_notification' => 'in:0,1',
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    
                    if (User::whereId($user_id)->exists()) {
                        
                        //initializing dynamic update variables
                        $data = $this->InitializingVariables($request, $user_id);
                        
                        //update user info
                        User::whereId($user_id)->update($data);
                        //return user data
                        $user_object = User::whereId($user_id)->withCount('posts')->withCount('followings')->withCount('followers')->first();
                        $user_object->pet_types = $user_object->PetTypes();
                        $user_object->account_types = [PROMOTION_USER_PET_SHOP, PROMOTION_USER_PET_CLINIC];
                        
                        self::$StatusCode = 200;
                        self::$Error = false;
                        self::$Msg = USER_UPDATE_200;
                        self::$Data = $user_object;
                    } else {
                        self::$StatusCode = 404;
                        self::$Error = true;
                        self::$Msg = USER_ID_404;
                        self::$Data = null;
                    }
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function update_password(Request $request)
        {
            try {
                $user_id = Auth::user()->id;
                $user_old_password = Auth::user()->password;
                
                $validator = Validator::make($request->all(), [
                    'old_password' => 'required',
                    'new_password' => 'min:6|required|confirmed|different:old_password',
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    
                    if (User::whereId($user_id)->exists()) {
                        
                        if (Hash::check($request->old_password, $user_old_password)) {
                            
                            //update user info
                            User::whereId($user_id)->update(['password' => Hash::Make($request->new_password)]);
                            
                            self::$StatusCode = 200;
                            self::$Error = false;
                            self::$Msg = USER_UPDATE_PASS_200;
                            self::$Data = null;
                        } else {
                            self::$StatusCode = 406;
                            self::$Error = true;
                            self::$Msg = USER_PASS_UPDATE_406;
                            self::$Data = null;
                        }
                    } else {
                        self::$StatusCode = 404;
                        self::$Error = true;
                        self::$Msg = USER_ID_404;
                        self::$Data = null;
                    }
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * @return \Illuminate\Http\JsonResponse
         */
        public function destroy()
        {
            try {
                $user_id = Auth::user()->id;
                if (User::whereId($user_id)->exists()) {
                    $user = User::find($user_id);
                    
                    //Remove related models info like posts, comments
                    $posts = Posts::whereUserId($user_id)->with('comments')->with('likes')->with('tags')->get();
                    
                    //check if there is any post
                    if ($posts->count() > 0) {
                        foreach ($posts as $post) {
                            if ($post) {
                                Tags::whereUserCreatorId($user_id)->delete();
                                Likes::whereUserId($user_id)->delete();
                                //chech if there if any comment
                                if ($post->comments->count() > 0) {
                                    foreach ($post->comments as $comment) {
                                        $comment->delete();
                                    }
                                }
                                //Remove post file
                                Delete_File($post->contents);
                                //Remove posts records
                                Posts::whereUserId($user_id)->delete();
                            }
                        }
                    }
                    
                    //Remove it's avatar
                    Delete_File($user->profile_photo);
                    
                    //delete old access tokens
                    Remove_Access_Token($user_id);
                    //Remove assigned role
                    $user->removeRole(NORMAL_USER_ROLE);
                    //Remove it's profile
                    User::whereId($user_id)->delete();
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = USER_DEL_200;
                    self::$Data = null;
                } else {
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = USER_ID_404;
                    self::$Data = null;
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * @param Request $request
         *
         * @return array
         */
        private function InitializingVariables(Request $request, $user_id)
        {
            $data = [];
            //----------- upload profile photo section -------------
            //Get User selected Info
            $User = User::select('id', 'mobile', 'email', 'mobile_confirmed', 'email_confirmed', 'profile_photo')->whereId($user_id)->first();
            //Get current user profile photo
            $current_profile_photo = $User->profile_photo;
            //upload user profile photo or keep it as it is.
            $data['profile_photo'] = Upload($request, 'profile_photo', $user_id, $current_profile_photo);
            //----------- upload profile photo section -------------
            
            //check if user is bussiness account
            if (Auth::user()->is_bussiness_account != 0) {
                
                if ($request->has('phone') && !is_null($request->phone) && $request->phone != NULL_VALUE && $request->phone != EMPTY_STRING)
                    $data['phone'] = $request->phone;
                
                if ($request->has('address') && !is_null($request->address) && $request->address != NULL_VALUE && $request->address != EMPTY_STRING)
                    $data['address'] = $request->address;
            }
            
            if ($request->has('username') && !is_null($request->username) && $request->username != NULL_VALUE && $request->username != EMPTY_STRING)
                $data['username'] = $request->username;
            
            if ($request->has('name') && !is_null($request->name) && $request->name != NULL_VALUE && $request->name != EMPTY_STRING)
                $data['name'] = $request->name;
            
            if ($request->has('gender') && !is_null($request->gender) && $request->gender != NULL_VALUE && $request->gender != EMPTY_STRING)
                $data['gender'] = $request->gender;
            
            if ($request->has('birth_date') && !is_null($request->birth_date) && $request->birth_date != NULL_VALUE && $request->birth_date != EMPTY_STRING)
                $data['birth_date'] = $request->birth_date;
            
            if ($request->has('bio') && !is_null($request->bio))
                $data['bio'] = $request->bio;
            
            //check if user is registered by mobile number
            if ($request->has('mobile') && !is_null($request->mobile) && $request->mobile != NULL_VALUE && $request->mobile != EMPTY_STRING)
                if (!$User->mobile_confirmed)
                    $data['mobile'] = $request->mobile;
                else if ($User->mobile != $request->mobile)
                    abort(406, USER_UPDATE_MOBILE_406);
            
            //check if user is registered by email address
            if ($request->has('email') && !is_null($request->email) && $request->email != NULL_VALUE && $request->email != EMPTY_STRING)
                if (!$User->email_confirmed)
                    $data['email'] = $request->email;
                else if ($User->email != $request->email)
                    abort(406, USER_UPDATE_EMAIL_406);
            
            if ($request->has('is_private') && !is_null($request->is_private) && $request->is_private != NULL_VALUE && $request->is_private != EMPTY_STRING)
                $data['is_private'] = $request->is_private;
            
            if ($request->has('website') && !is_null($request->website) && $request->website != NULL_VALUE)
                $data['website'] = $request->website;
            
            if ($request->has('pet_type') && !is_null($request->pet_type) && $request->pet_type != NULL_VALUE && $request->pet_type != EMPTY_STRING)
                $data['pet_type'] = $request->pet_type;
            
            //Search for breed if not exist create one and store it's id here.
            if ($request->has('breeds') && !is_null($request->breeds) && $request->breeds != NULL_VALUE && $request->breeds != EMPTY_STRING) {
                $breedObj = AnimalBreeds::whereTitle('like', '%' . $request->breeds . '%')->first();
                if (!is_null($breedObj)) {
                    $data['breeds'] = $breedObj->id;
                } else {
                    $breedID = AnimalBreeds::insertGetId(['cat_id' => $request->pet_type, 'title' => $request->breeds]);
                    $data['breeds'] = $breedID;
                }
            }
            
            if ($request->has('sterilized') && !is_null($request->sterilized) && $request->sterilized != NULL_VALUE && $request->sterilized != EMPTY_STRING)
                $data['sterilized'] = $request->sterilized;
            
            if ($request->has('password') && !is_null($request->password) && $request->password != NULL_VALUE && $request->password != EMPTY_STRING)
                $data['password'] = Hash::Make($request->password);
            
            if ($request->has('county_id') && !is_null($request->county_id) && $request->county_id != NULL_VALUE && $request->county_id != EMPTY_STRING)
                $data['county_id'] = $request->county_id;
            
            $data['ip_address'] = array_key_exists("HTTP_X_FORWARDED_FOR", $_SERVER) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $request->ip();
            
            if ($request->has('lat') && !is_null($request->lat) && $request->lat != NULL_VALUE && $request->lat != EMPTY_STRING)
                $data['lat'] = $request->lat;
            
            if ($request->has('long') && !is_null($request->long) && $request->long != NULL_VALUE && $request->long != EMPTY_STRING)
                $data['long'] = $request->long;
            
            if ($request->has('detail_id') && !is_null($request->detail_id) && $request->detail_id != NULL_VALUE && $request->detail_id != EMPTY_STRING)
                $data['detail_id'] = $request->detail_id;
            
            if ($request->has('saved_post') && !is_null($request->saved_post) && $request->saved_post != NULL_VALUE && $request->saved_post != EMPTY_STRING)
                $data['saved_post'] = $request->saved_post;
            
            if ($request->has('searches') && !is_null($request->searches) && $request->searches != NULL_VALUE && $request->searches != EMPTY_STRING)
                $data['searches'] = $request->searches;
            
            if ($request->has('reagent_code') && !is_null($request->reagent_code) && $request->reagent_code != NULL_VALUE && $request->reagent_code != EMPTY_STRING)
                $data['reagent_code'] = $request->reagent_code;
            
            if ($request->has('reagent_id') && !is_null($request->reagent_id) && $request->reagent_id != NULL_VALUE && $request->reagent_id != EMPTY_STRING)
                $data['reagent_id'] = $request->reagent_id;
            
            if ($request->has('blocked_users') && !is_null($request->blocked_users) && $request->blocked_users != NULL_VALUE && $request->blocked_users != EMPTY_STRING)
                $data['blocked_users'] = $request->blocked_users;
            
            if ($request->has('is_pet_owner') && !is_null($request->is_pet_owner) && $request->is_pet_owner != NULL_VALUE && $request->is_pet_owner != EMPTY_STRING)
                $data['is_pet_owner'] = $request->is_pet_owner;
            
            // get the names of the user's roles
            $user_role = Auth::user()->getRoleNames()->first(); // Returns a string);
            if ($user_role == PROMOTION_USER_ROLE) {
                if ($request->has('account_type') && !is_null($request->account_type) && $request->account_type != NULL_VALUE && $request->account_type != EMPTY_STRING)
                    $data['account_type'] = $request->account_type;
            }
            
            if ($request->has('expiration_at') && !is_null($request->expiration_at) && $request->expiration_at != NULL_VALUE && $request->expiration_at != EMPTY_STRING)
                $data['expiration_at'] = $request->expiration_at;
            
            if ($request->has('receive_notification') && !is_null($request->receive_notification) && $request->receive_notification != NULL_VALUE && $request->receive_notification != EMPTY_STRING)
                $data['receive_notification'] = $request->receive_notification;
            
            if ($request->has('show_me_on_map') && !is_null($request->show_me_on_map) && $request->show_me_on_map != NULL_VALUE && $request->show_me_on_map != EMPTY_STRING)
                $data['show_me_on_map'] = $request->show_me_on_map;
            
            return $data;
        }
        
        public function get_blocked_users()
        {
            try {
                $my_id = Auth::user()->id;
                $blocked_users_uid_collection = BlockedUsers::where('blocker_uid', $my_id)->get();
                
                $Blocked_Users = [];
                if ($blocked_users_uid_collection->count() > 0) {
                    foreach ($blocked_users_uid_collection as $uid_object) {
                        $Blocked_Users[] = User::find($uid_object['target_uid']);
                    }
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $Blocked_Users;
                } else {
                    //prepare response
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = NO_BLOCKED_USER_404;
                    self::$Data = null;
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /*
        * When you block someone,
        * that person won't be able to find your profile,
        * posts or story.
        * the blocked user will be removed from following if it's in followings
        * remove blocked user from current user following list
        * remove it from home posts
        * don't receive any comment or direct or allow it to tag or mention the current user
        *  People aren't notified when you block them.
        * Yes, someone you blocked can mention your username, but this mention won't appear in your Activity.
        * Blocked user should be able to follow current user again
       */
        public function block(Request $request)
        {
            try {
                $validator = Validator::make($request->all(), [
                    'user_id' => 'required|exists:users,id',
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    if (Auth::user()->id != $request->user_id) {
                        $my_user_id = Auth::user()->id;
                        $myuser = User::findOrFail($my_user_id);
                        
                        if (!BlockedUsers::where('blocker_uid', $my_user_id)->where('target_uid', $request->user_id)->exists()) {
                            
                            //unfollow target user
                            $myuser->followings()->detach([$request->user_id]);
                            $myuser->followers()->detach([$request->user_id]);
                            
                            //stop target user from sending comment and direct
                            //TODO stop target user from sending comment and direct
                            
                            //block target user from viewing my profile
                            //TODO block target user from viewing my profile
                            
                            //Store target user id in blocked users table
                            $myuser->blocked()->attach($request->user_id);
                            
                            $blocked_user = User::findOrFail($request->user_id);
                            $blocked_user->is_blocked = true;
                            
                            //prepare response
                            self::$StatusCode = 200;
                            self::$Error = false;
                            self::$Msg = USER_BLOCKED_200;
                            self::$Data = $blocked_user;
                        } else {
                            //prepare response
                            self::$StatusCode = 406;
                            self::$Error = true;
                            self::$Msg = USER_ALREADY_BLOCKED_406;
                            self::$Data = null;
                        }
                    } else {
                        //prepare response
                        self::$StatusCode = 406;
                        self::$Error = true;
                        self::$Msg = USER_SELF_BLOCK_ERROR_406;
                        self::$Data = null;
                    }
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function unblock(Request $request)
        {
            try {
                $validator = Validator::make($request->all(), [
                    'user_id' => 'required|exists:users,id',
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    $my_user_id = Auth::user()->id;
                    $myuser = User::findOrFail($my_user_id);
                    
                    //Store target user id in blocked users table
                    $myuser->blocked()->detach($request->user_id);
                    
                    $unblocked_user = User::findOrFail($request->user_id);
                    $unblocked_user->is_blocked = false;
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = USER_UNBLOCKED_200;
                    self::$Data = $unblocked_user;
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function profile($user_id)
        {
            try {
                $my_id = Auth::user()->id;
                
                $user = User::select(
                    "id",
                    "username",
                    "name",
                    "bio",
                    "website",
                    "profile_photo",
                    "is_private",
                    "is_bussiness_account",
                    "phone",
                    "address"
                )
                    ->whereId($user_id)
                    ->first();
                
                if (!$user) {
                    
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = USER_ID_404;
                    self::$Data = null;
                    
                } else {
                    //check if target user is being blocked
                    $blocked = BlockedUsers::whereBlockerUid($my_id)->whereTargetUid($user_id)->first();
                    if (!is_null($blocked))
                        $user->is_blocked = true;
                    else
                        $user->is_blocked = false;
                    
                    $user->posts_count = $user
                        ->posts()
                        ->count();
                    
                    $user->followers_count = $user
                        ->followers()
                        ->where("accepted", 1)
                        ->count();
                    
                    $user->followings_count = $user
                        ->followings()
                        ->where("accepted", 1)
                        ->count();
                    
                    $user->can_edit_profile = ($user->id == $my_id) ? 1 : 0;
                    $user->can_see_private_info = 0;
                    if (
                        $user->is_private == 0 ||
                        $user->follow_status == 2 ||
                        $user->id == $my_id
                    )
                        $user->can_see_private_info = 1;
                    
                    if ($user->can_see_private_info == 1)
                        $user->load([
                            "posts" => function ($query) {
                                $query->with([
                                    'user' => function ($query) {
                                        $query->select(
                                            "id",
                                            "username",
                                            "name",
                                            "profile_photo"
                                        )
                                            ->withCount('posts')
                                            ->withCount('followers')
                                            ->withCount('followings');
                                    }])
                                    ->with(["tags", "peoples"])
                                    ->withCount('likes')
                                    ->withCount('comments')
                                    ->take(30)
                                    ->orderBy('created_at', 'desc');
                            }
                        ]);
                    else
                        $user->posts = [];
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $user;
                }
                
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        
        public function search($UserSearchParam)
        {
            try {
                $my_id = Auth::user()->id;
                $users = User::select(
                    "id",
                    "username",
                    "bio",
                    "website",
                    "name",
                    "profile_photo",
                    "is_private"
                )
                    ->where(function ($query) use ($UserSearchParam) {
                        $query->where('username', 'LIKE', '%' . $UserSearchParam . '%')
                            ->orWhere('name', 'LIKE', '%' . $UserSearchParam . '%');
                    })->withCount('posts')
                    ->withCount(['followers' => function ($query) {
                        $query->where('accepted', 1);
                    }])
                    ->withCount(['followings' => function ($query) {
                        $query->where('accepted', 1);
                    }])
                    ->get();
                
                $final_users = $users->reject(function ($user) {
                    return $user->hasRole(ADMIN_ROLE);
                });
                //convert collection into array
                $final_users = $final_users->toArray();
                //reset array key values
                $final_users = array_values($final_users);
                
                if (!$final_users) {
                    
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = USER_ID_404;
                    self::$Data = null;
                    
                } else {
                    //TODO fix this part
                    /*
                     $user->follow_status = "un-follow";
    
                     $is_i_follow = $user
                         ->followers()
                         ->where("follower_id", $my_id)
                         ->first();
    
                     if ($is_i_follow) {
                         $user->follow_status = "requested";
    
                         if ($is_i_follow->pivot->accepted)
                             $user->follow_status = "follow";
                     }
    
                     $user->can_edit_profile = ($user->id == $my_id) ? 1 : 0;
                     $user->can_see_private_info = 0;
                     if (
                         $user->is_private == 0 ||
                         $user->follow_status == "follow" ||
                         $user->id == $my_id
                     )
                         $user->can_see_private_info = 1;
    
                     if ($user->can_see_private_info == 1)
                         $user->load([
                             "posts" => function ($query) {
                                 $query
                                     ->with(["tags"])
                                     ->take(30)
                                     ->orderBy('created_at', 'desc');
                             }
                         ]);
                     else
                         $user->posts = [];
                    */
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $final_users;
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function switch_to_bussiness_account(Request $request)
        {
            try {
                $validator = Validator::make($request->all(), [
                    'account_type' => Rule::in([
                        'required',
                        PROMOTION_USER_PET_SHOP,
                        PROMOTION_USER_PET_CLINIC
                    ]),
                    'phone' => 'required',
                    'address' => 'required',
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    
                    $uid = Auth::user()->id;
                    $user = User::findOrFail($uid);
                    $user->removeRole(NORMAL_USER_ROLE);
                    $user->assignRole(PROMOTION_USER_ROLE);
                    
                    $user->account_type = $request->account_type;
                    $user->phone = $request->phone;
                    $user->address = $request->address;
                    $user->is_bussiness_account = 1;
                    $user->save();
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = USER_PROMOTED_200;
                    self::$Data = $user;
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function switch_back_to_general_account(Request $request)
        {
            try {
                $uid = Auth::user()->id;
                $user = User::findOrFail($uid);
                $user->removeRole(PROMOTION_USER_ROLE);
                $user->assignRole(NORMAL_USER_ROLE);
                
                $user->account_type = null;
                $user->is_bussiness_account = 0;
                $user->save();
                
                //prepare response
                self::$StatusCode = 200;
                self::$Error = false;
                self::$Msg = USER_DEMOTED_200;
                self::$Data = $user;
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Deactive Account
         * After deactivation -> Hidden :
         * user profile
         * user posts
         * user comments
         * user likes
         * untile user log back in to reactivate account
         *
         * logout the user and remove it's tokens
         *
         * to reactivate user account should change it's param to 0 on login.
         */
        public function deactive()
        {
            try {
                $uid = Auth::user()->id;
                $user = User::findOrFail($uid);
                $user->is_deactive = 1;//deactive user
                $user->save();
                
                //log out user
                //check if user auth is valid then delete all his/her access tokens
                if (Auth::check()) {
                    Auth::user()->AauthAcessToken()->delete();
                }
                
                //prepare response
                self::$StatusCode = 200;
                self::$Error = false;
                self::$Msg = USER_DEACTIVE_200;
                self::$Data = $user;
                
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function search_by_location(Request $request)
        {
            try {
                
                $distance = 10;
                if ($request->get("distance"))
                    $distance = $request->get("distance");
                
                $my_id = Auth::user()->id;
                $users =
                    DB::
                    table(DB::raw("(SELECT *, (3956 * 2 * ASIN(SQRT(POWER(SIN((" . $request->lat . "- `lat`) *  pi()/180 / 2), 2) +COS(" . $request->lat . " * pi()/180) * COS(lat * pi()/180) * POWER(SIN(( " . $request->long . " - `long`) * pi()/180 / 2), 2) ))) as `distance` FROM `users`) as users2 "))
                        ->leftJoin("animal_breeds", "breeds", "=", "animal_breeds.id")
                        ->select(
                            "users2.id",
                            "username",
                            "bio",
                            "website",
                            "lat",
                            "long",
                            "name",
                            "profile_photo",
                            "distance",
                            "pet_type",
                            "animal_breeds.title as breeds",
                            "gender",
                            "sterilized",
                            "is_private"
                        )
                        ->selectRaw("CEILING((YEAR(NOW()) - YEAR(`birth_date`))) AS age")
                        ->selectRaw("(SELECT `icon` FROM `animal_categories` WHERE `animal_categories`.`id` = `users2`.`pet_type`) AS marker")
                        ->selectRaw("(SELECT COUNT(*) FROM `posts` WHERE `posts`.`user_id` = `users2`.`id`) AS posts")
                        ->selectRaw("(SELECT COUNT(*) FROM `user_following` WHERE `user_following`.`follower_id` = `users2`.`id` AND `accepted` = 1) AS followings")
                        ->selectRaw("(SELECT COUNT(*) FROM `user_following` WHERE `user_following`.`following_id` = `users2`.`id` AND `accepted` = 1) AS followers")
                        ->where("distance", "<", $distance);
                
                //check for those user which wants to show on map
                $users->where("show_me_on_map", 1);
                
                if ($request->get("pet_type"))
                    $users->where("pet_type", $request->get("pet_type"));
                
                if ($request->get("breeds")) {
                    $users
                        ->where("animal_breeds.title", "like", "%" . $request->get("breeds") . "%");
                }
                
                if ($request->get("account_type"))
                    $users->where("account_type", $request->get("account_type"))
                        ->where("is_bussiness_account", 1);
                
                $users = $users->get();
                
                if (!$users) {
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = "";
                    self::$Data = null;
                    
                } else {
                    
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $users;
                }
            } catch (\Throwable $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage() . " " . $e->getFile() . " " . $e->getLine();
                self::$Data = null;
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        
        public function user_activity()
        {
            try {
                $user_id = Auth::user()->id;
                $user = User::find($user_id);
                $user->Notifications;
//                $user->unreadNotifications->markAsRead();
                
                //prepare response
                self::$StatusCode = 200;
                self::$Error = false;
                self::$Msg = null;
                self::$Data = $user;
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
    }
