<?php
    
    namespace App\Http\Controllers\User;
    
    use App\Models\Events;
    use App\Models\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;
    use Validator;
    
    class EventsController extends Controller
    {
        protected static $StatusCode = 0;
        protected static $Error;
        protected static $Msg = null;
        protected static $Data = null;
        
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            try {
                if (Events::whereUserId(Auth::user()->id)->exists()) {
                    
                    $result = User::whereId(Auth::user()->id)->with('Events')->first();
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $result;
                } else {
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = NO_EVENT_404;
                    self::$Data = null;
                }
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            try {
                $validator = Validator::make($request->all(), [
                    'event_name' => 'required|max:255',
                    'start_date' => 'required|date',
                    'end_date' => 'required|date|after_or_equal:start_date'
                ]);
                
                if ($validator->fails()) {
                    abort(406, $validator->errors()->first());
                } else {
                    
                    $event = new Events();
                    $event->user_id = Auth::user()->id;
                    $event->event_name = $request->event_name;
                    $event->start_date = $request->start_date;
                    $event->end_date = $request->end_date;
                    $event->save();
                    
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = EVENT_STORE_200;
                    self::$Data = $event;
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            try {
                if (Events::whereUserId(Auth::user()->id)->whereId($id)->exists()) {
                    
                    $event = Events::find($id);
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $event;
                } else {
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = EVENT_ID_404;
                    self::$Data = null;
                }
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            try {
                if (Events::whereUserId(Auth::user()->id)->whereId($id)->exists()) {
                    
                    $validator = Validator::make($request->all(), [
                        'event_name' => 'max:255',
                        'start_date' => 'date',
                        'end_date' => 'date|after_or_equal:start_date'
                    ]);
                    
                    if ($validator->fails()) {
                        abort(406, $validator->errors()->first());
                    } else {
                        
                        if (Events::whereId($id)->exists()) {
                            
                            $params = [];
                            if ($request->has('event_name'))
                                $params['event_name'] = $request->event_name;
                            
                            if ($request->has('start_date'))
                                $params['start_date'] = $request->start_date;
                            
                            if ($request->has('end_date'))
                                $params['end_date'] = $request->end_date;
                             
                            Events::whereUserId(Auth::user()->id)->whereId($id)->update($params);
                            
                            self::$StatusCode = 200;
                            self::$Error = false;
                            self::$Msg = EVENT_UPDATE_200;
                            self::$Data = null;
                        } else {
                            self::$StatusCode = 404;
                            self::$Error = true;
                            self::$Msg = EVENT_ID_404;
                            self::$Data = null;
                        }
                    }
                }
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            try {
                if (Events::whereUserId(Auth::user()->id)->whereId($id)->exists()) {
                    
                    Events::whereUserId(Auth::user()->id)->whereId($id)->delete();
                    
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = EVENT_DEL_200;
                    self::$Data = null;
                } else {
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = EVENT_ID_404;
                    self::$Data = null;
                }
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 500;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
    }
