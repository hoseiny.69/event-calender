<?php
    
    namespace App\Http\Controllers\Auth;
    
    use App\Models\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Hash;
    use Validator;
    use Illuminate\Support\Facades\Auth;
    
    class AuthController extends Controller
    {
        protected static $StatusCode = 0;
        protected static $Error;
        protected static $Msg = null;
        protected static $Data = null;
        
        /**
         * Login user
         *
         * @param $request Request
         */
        public function login(Request $request)
        {
            try {
                RemoveZeroFromMobile($request);
                
                $validator = Validator::make($request->all(), [
                    'mobile' => 'required|min:10|max:11|regex:/[1-9]{1,11}/|exists:users,mobile',
                    'password' => 'required'
                ]);
                
                if ($validator->fails()) {
                    abort(400, $validator->errors()->first());
                } else {
                    
                    if ($request->has('mobile')) {
                        $login = User::where('mobile', $request->mobile)->whereIsActivated(1)->whereMobileConfirmed(1)->first();
                    }
                    
                    if (!$login) {
                        //prepare response
                        self::$StatusCode = 406;
                        self::$Error = true;
                        self::$Msg = USER_ACTIVE_406;
                        self::$Data = null;
                    } else {
                        
                        //check if user input password is matching the one stored in db
                        if (Hash::check($request->password, $login->password)) {
                            //issue user access token
                            $token_result = GetToken($login);
                            $token_result->user = $login;
                            
                            //prepare response
                            self::$StatusCode = 200;
                            self::$Error = false;
                            self::$Msg = null;
                            self::$Data = $token_result;
                            
                        } else {
                            //prepare response
                            self::$StatusCode = 401;
                            self::$Error = true;
                            self::$Msg = USER_AUTH_401;
                            self::$Data = null;
                        }
                    }
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        /**
         * Get Authenticated User
         */
        public function get_user()
        {
            try {
                $user = User::find(Auth::user()->id);
                
                if ($user) {
                    //prepare response
                    self::$StatusCode = 200;
                    self::$Error = false;
                    self::$Msg = null;
                    self::$Data = $user;
                } else {
                    //prepare response
                    self::$StatusCode = 404;
                    self::$Error = true;
                    self::$Msg = USER_404;
                    self::$Data = null;
                }
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
        
        public function logout()
        {
            try {
                
                //check if user auth is valid then delete all his/her access tokens
                if (Auth::check()) {
                    Auth::user()->token()->delete();
                }
                
                //return success message for log out
                self::$StatusCode = 200;
                self::$Error = false;
                self::$Msg = USER_LOGOUT_200;
                self::$Data = null;
                
            } catch (\Exception $e) {
                //=========== Get Error Exception Message ============
                self::$StatusCode = 400;
                self::$Error = true;
                self::$Msg = $e->getMessage();
                self::$Data = null;
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
                //=========== Get Error Exception Message ============
            } finally {
                return $this->JRES(self::$StatusCode, self::$Error, self::$Msg, self::$Data);
            }
        }
    }
