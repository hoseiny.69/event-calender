<?php
    
    namespace App\Http\Controllers;
    
    use Laravel\Lumen\Routing\Controller as BaseController;
    
    class Controller extends BaseController
    {
        public function JRES($code, $error, $message = null, $data = null)
        {
            return response()->json([
                'code' => $code,
                'error' => $error,
                'message' => $message ?: null,
                'data' => $data ?: null,
            ], 200);
        }
    }
