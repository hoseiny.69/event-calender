<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OAuthClient extends Model
{

    protected $table = 'oauth_clients';

    protected $fillable = [];

    protected $dates = [];

    public static $rules = [
        // Validation rules
    ];

    // Relationships
    
    public function getCreatedAtAttribute($value)
    {
        return strtotime($value);
    }
    
    public function getUpdatedAtAttribute($value)
    {
        return strtotime($value);
    }
}
