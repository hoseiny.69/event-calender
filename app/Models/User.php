<?php

namespace App\Models;

use App\Scopes\IsDeactiveScope;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;


class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use HasApiTokens, Authenticatable, Authorizable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'username',
        'name',
        'gender',
        'birth_date',
        'mobile',
        'password',
        'ip_address',
        'mobile_confirmed',
        'is_activated',
        'is_deactive',
        'expiration_at',
        'deleted_at',
    ];
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];
    

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new IsDeactiveScope());
    }
    
    public function Events()
    {
        return $this->hasMany(Events::class);
    }

    public function findForPassport($username)
    {
        return $this->where(function ($query) use ($username) {
            $query->where('mobile', $username);
        })->first();
    }
    
    public function AauthAcessToken()
    {
        return $this->hasMany(OauthAccessToken::class);
    }
    

    public function getCreatedAtAttribute($value)
    {
        return strtotime($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return strtotime($value);
    }
}
