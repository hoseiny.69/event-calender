<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name',
        'start_date',
        'end_date',
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
}
